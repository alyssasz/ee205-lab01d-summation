///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 01b - Summation
//
// Usage:  summation n
//   n:  Sum the digits from 1 to n
//
// Result:
//   The sum of the digits from 1 to n
//
// Example:
//   $ summation 6
//   The sum of the digits from 1 to 6 is 21
//
// @author Alyssa Zhang <alyssasz@hawaii.edu>
// @date   14 Jan 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    
   argv[1] = "6"; /* change this input for the value you want to sum up to */
   int n = atoi(argv[1]);
   int sum = 0;

   for (int i = 1; i <= n; i++ ) {
      sum = sum + i; 
   }

   printf("%d\n", sum); 

   return 0;
}
